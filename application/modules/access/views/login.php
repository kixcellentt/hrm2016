<!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="" method="post">
        <h3 class="form-title">Login to your account</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>
            Enter any username and password. </span>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Username / Email</label>
            <div class="input-icon" data-error-container="#input-user-error">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" name="data[Username]" id="input-user" placeholder="Username or Email" autocomplete="off">
            </div>
            <div id="input-user-error"></div>
        </div>
        <div class="form-group" >
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon" data-error-container="#input-password-error">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" name="data[Password]" id="input-password" placeholder="Password" autocomplete="off">
            </div>
            <div id="input-password-error"></div>
        </div>
        <div class="form-actions">
            <label class="checkbox">
            <input type="checkbox" name="remember" value="1"/> Remember me </label>
            <button type="submit" class="btn green-haze pull-right">
            Login <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        <!-- <div class="login-options">
            <h4>Or login with</h4>
            <ul class="social-icons">
                <li>
                    <a class="facebook" data-original-title="facebook" href="#">
                    </a>
                </li>
                <li>
                    <a class="twitter" data-original-title="Twitter" href="#">
                    </a>
                </li>
                <li>
                    <a class="googleplus" data-original-title="Goole Plus" href="#">
                    </a>
                </li>
                <li>
                    <a class="linkedin" data-original-title="Linkedin" href="#">
                    </a>
                </li>
            </ul>
        </div> -->
        <div class="forget-password">
            <h4>Forgot your password ?</h4>
            <p>
                no worries, click <a href="<?=base_url('access/forgot_password');?>" id="forget-password">
               here </a>
               to reset your password.
            </p>
        </div>
        <div class="create-account">
            <p>
                Don't have an account yet ?&nbsp; <a href="<?=base_url('access/register');?>" id="register-btn">
               Create an account </a>
            </p>
        </div>
    </form>
<!-- END LOGIN FORM -->