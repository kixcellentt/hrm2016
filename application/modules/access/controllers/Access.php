<?php
/**
 * Access.  This is use for user login, registration, and forgot password.
 * @author redcrisostomo
 */
class Access extends MY_Controller
{
    public function __construct() 
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->login();
    }
    
    /**
     * Login
     */
    public function login()
    {
        if($this->session->userdata('hrm_is_logged')==TRUE) redirect(base_url());
        
        $this->template
            ->title('Login Access','Login Page')
            ->set_layout('access')
            ->build('access/login');
    }
    
    /**
     * Logout
     */
    public function logout()
    {
        session_destroy();
        redirect(base_url());
    }
    
    /**
     * Forgot Password
     */
    public function forgot_password()
    {
        $this->template
            ->title('Forgot Password','Login Page')
            ->set_layout('access')
            ->build('access/forgot_password');
    }
    
    /**
     * Register
     */
    public function register()
    {
        $this->template
            ->title('Register','Register Page')
            ->set_layout('access')
            ->build('access/register');
    }
    /**
     * Utility Method to validate the login
     * @see assets/custom/login/login-validate.js
     * @return void
     */
    public function validateLogin()
    {
        if(!$this->input->post('data')){echo "false"; return;}
        $data = $this->input->post('data');
        $username = $data['User'];
        $password = crypt($data['Password'], $this->config->item('encryption_key'));

        if($query = $this->db->query("call getUserByUsername('$username','$password')"))
        {
            $result = $query->result_array();
            /*set session data*/
            $newsession = array(
                'hrm_user_id'  => $result[0]['UserID'],
                'hrm_user_role_id' => $result[0]['UserRoleID'],
                'hrm_person_id' => $result[0]['PersonID'],
                'hrm_fullname' => $result[0]['FirstName'].' '.$result[0]['LastName'],
                'hrm_profile_pic' => $result[0]['ProfilePicture'],
                'hrm_email'     => $result[0]['UserEmail'],
                'hrm_is_logged' => TRUE
            );
            $this->session->set_userdata($newsession);
            echo "true"; return;
        }
        echo "false"; return;
    }
}
