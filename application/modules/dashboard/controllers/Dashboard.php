<?php 
/**
 * Dashboard
 *
 * @author redcrisostomo
 */
class Dashboard extends MY_Controller
{
    public function __construct() {
        parent::__construct();
        $this->checkIsLogged();
    }
    
    /**
     * Primary Dashboard 
     */
    public function index()
    {
        $this->template
            ->title('Dashboard','HRM2016')
            ->build('dashboard/dashboard');
    }
    
    /**
     * Secondary Dashboard
     */
    public function dashboard2()
    {
        $this->template
            ->title('Dashboard2','HRM2016')
            ->build('dashboard/dashboard2');
    }
}
