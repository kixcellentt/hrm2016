<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of MY_Controller
 *
 * @author redcrisostomo
 */
class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Check if the user is logged
     * @return void
     */
    public function checkIsLogged()
    {
        if($this->session->userdata('hrm_is_logged')!=TRUE)
        {
            redirect(base_url('access'));
            return;
        }
    }
}
