###################
About HRM2016
###################

HRM2016 is a web-based application system for company or organization
who wants to automate their process in Human Resource Management. 
Its goal is to provide the employees a facility to apply their requests online 
such as leave application, travel request, overtime request, request for 
time adjustment, and others. This will also facilitate faster and automated 
computation of attendance and payroll.

*******************
Release Information
*******************

This repo contains in-development code for future releases.


*******************
Server Requirements
*******************

PHP version 5.4 or newer is recommended.

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

The installation and configuration will be provided later.

*******
License
*******

GNU General Public License v3 <https://www.gnu.org/licenses/gpl-3.0.en.html>.

*********
Resources
*********

-  `Codeigniter <https://codeigniter.com>`_
-  `Codeigniter Template Master <https://github.com/philsturgeon/codeigniter-template>`_
-  `Metronic Admin Theme <http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469>`_

Report security issues to us, thank you.